import React, { Component } from 'react';
import { CssBaseline, Container, MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import axios from 'axios';

import TopHeader from './components/TopHeader';
import BottomNav from './components/BottomNav';
import GlobalContext from './GlobalContext';

import DevPage from './pages/DevPage/DevPage';
import AuthPage from './pages/AuthPage/AuthPage';
import HomePage from './pages/HomePage/HomePage';
import SearchPage from './pages/SearchPage/SearchPage';
import ProfilePage from './pages/ProfilePage/ProfilePage';
import UpgradePage from './pages/UpgradePage/UpgradePage';
import MessagesPage from './pages/MessagePage/MessagesPage';
import SettingsPage from './pages/SettingsPage/SettingsPage';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ff69b4'
    },
    secondary: {
      main: '#be69ff'
    }
  }
})

function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        localStorage.getItem('isl') ? (children) : (
          <Redirect
            to={{
              pathname: "/auth",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

export default class Root extends Component {
  state = {
    isLoggedIn   : localStorage.getItem('isl'),
    type         : null,
    avatar       : null,
    defaultAvatar: null,
    uid          : null,
    online       : true,
    params       : {},
    status       : null,
  }

  componentDidMount() {
    const fetchData = async () => {
      const result = await axios.post('/api/getuserinfo', {
          uid: localStorage.getItem('uid')
      })
      
      this.setState({
        ...result.data
      });
    };
    fetchData();
  }
  
  render() {   
    return (
      <MuiThemeProvider theme={theme}>
          <CssBaseline />
          <GlobalContext.Provider value={{
              state: this.state,
              setState: state => (this.setState(state))
            }}>
            <Router>
              <TopHeader />
              <Container maxWidth="lg">
                <Switch>
                  <Route exact path="/" component={HomePage}/>
                  <Route path="/search" component={SearchPage}/>
                  <Route path="/auth" component={AuthPage}/>
                  <PrivateRoute path="/messages">
                    { true ? <DevPage /> : <MessagesPage />}
                  </PrivateRoute>
                  <PrivateRoute path="/profile">
                    <ProfilePage />
                  </PrivateRoute>
                  <PrivateRoute path="/settings">
                    <SettingsPage />
                  </PrivateRoute>
                  <PrivateRoute path="/upgrade">
                    <UpgradePage />
                  </PrivateRoute>
                </Switch>
              </Container>
              <BottomNav />
            </Router>
          </GlobalContext.Provider>
      </MuiThemeProvider>
    );
  }
}