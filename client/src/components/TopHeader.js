import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    root: {
      width: '100%',
      color: 'hotpink',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      fontSize: '5vmin',
      fontFamily: 'monospace',
      fontWeight: 900,
      userSelect: 'none'
    },
    span: {
      fontSize: '2.8vmin',
      userSelect: 'none'
    }
  }));

export default function TopHeader() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <span role="img" aria-label="diamond" className={classes.span}>💎</span>
            PHODEL
            <span role="img" aria-label="diamond" className={classes.span}>💎</span>
        </div>
    )
}
