import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles({
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    margin: 10,
    width: 100,
    height: 100,
  },
});

export default function ImageAvatars(props) {
  const classes = useStyles();  

  return (
    <Grid container justify={props.justify === 'center' ? 'center' : 'flex-start'} alignItems={props.alignItems === 'center' ? 'center' : 'flex-start'}>
      <Avatar alt="Remy Sharp" src={props.imgSrc} className={props.size === 'lg' ? classes.bigAvatar : classes.avatar} />
    </Grid>
  );
}