import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import { Link } from 'react-router-dom';

import HomeIcon from '@material-ui/icons/Home';
import SearchIcon from '@material-ui/icons/Search';
import MailIcon from '@material-ui/icons/Mail';
import PersonIcon from '@material-ui/icons/Person';

const useStyles = makeStyles({
  root: {
    width: '100%',
    background: '#eee',
    position: "fixed",
    bottom: 0,
    left: 0
  },
});

export default function BottomNav() {
  const classes = useStyles();
  const [value, setValue] = React.useState('recents');

  return (
    <BottomNavigation 
    value={value}
    onChange={(event, newValue) => {
      setValue(newValue);
    }}
    showLabels
    className={classes.root}>
      <BottomNavigationAction label="Главная" value="home" icon={<HomeIcon />} component={Link} to="/" />
      <BottomNavigationAction label="Поиск" value="search" icon={<SearchIcon />} component={Link} to="/search" />
      <BottomNavigationAction label="Сообщения" value="messages" icon={<MailIcon />} component={Link} to="/messages" />
      <BottomNavigationAction label="Профиль" value="profile" icon={<PersonIcon />} component={Link} to="/profile" />
    </BottomNavigation>
  );
}