import React from 'react'

export default function DevPage() {
    return (
        <div style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            height: "40vh",
            lineHeight: .2
        }}>
            <h1>This page is currently developing <span role='img' aria-label='sign'>😣</span></h1>
            <h1>Cya later<span role='img' aria-label='wink'>😉</span></h1>
        </div>
    )
}
