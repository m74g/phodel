import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, MenuItem, Select, FormControl, InputLabel, Checkbox, FormControlLabel, Button } from '@material-ui/core';
import axios from 'axios';
import GlobalContext from '../../GlobalContext';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(2),
    width: '90%',
  },
  formControl: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(2),
    width: '90%',
  },
  checkbox: {
    margin: theme.spacing(3)
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
  button: {
    width: '90%',
    marginLeft: theme.spacing(3),
    color: '#fff'
  },
}));

export default function SettingsPageModel() {
    const classes = useStyles();

    useEffect( () => {
        document.title = 'Настройки профиля';    
    }, [])

    return (
        <GlobalContext.Consumer>
            {context => {

                const handleChange = name => event => {
                    context.setState({
                        params: {
                            ...context.state.params,
                            [name]: event.target.value
                        }
                    })
                };

                const handleChangeCheckbox = name => event => {
                    context.setState({
                        params: {
                            ...context.state.params,
                            [name]: event.target.checked
                        }
                    })
                };                

                return (
                    <form className={classes.root} autoComplete="off">
                        <TextField
                            id="name"
                            label="Имя"
                            className={classes.textField}
                            value={context.state.params.name || ''}
                            onChange={handleChange('name')}
                            margin="normal"
                            inputProps={{
                                name: 'name',
                                id: 'name',
                        }} />
                        
                        <TextField
                            id="surname"
                            label="Фамилия"
                            className={classes.textField}
                            value={context.state.params.surname || ''}
                            onChange={handleChange('surname')}
                            margin="normal"
                            inputProps={{
                                name: 'surname',
                                id: 'surname',
                        }} />
                        
                        <TextField
                            id="age"
                            label="Возраст"
                            className={classes.textField}
                            value={context.state.params.age || ''}
                            onChange={handleChange('age')}
                            margin="normal"
                            inputProps={{
                                name: 'age',
                                id: 'age',
                        }} />

                        <TextField
                            id="experience"
                            label="Опыт (лет)"
                            className={classes.textField}
                            value={context.state.params.experience || ''}
                            onChange={handleChange('experience')}
                            margin="normal"
                            inputProps={{
                                name: 'experience',
                                id: 'experience',
                        }} />
                        
                        <TextField
                            id="height"
                            label="Рост"
                            className={classes.textField}
                            value={context.state.params.height || ''}
                            onChange={handleChange('height')}
                            margin="normal"
                            inputProps={{
                                name: 'height',
                                id: 'height',
                        }} />
                
                        <TextField
                            id="weight"
                            label="Вес"
                            className={classes.textField}
                            value={context.state.params.weight || ''}
                            onChange={handleChange('weight')}
                            margin="normal"
                            inputProps={{
                                name: 'weight',
                                id: 'weight',
                        }} />
                
                        <TextField
                            id="bust"
                            label="Обхват груди"
                            className={classes.textField}
                            value={context.state.params.bust || ''}
                            onChange={handleChange('bust')}
                            margin="normal"
                            inputProps={{
                                name: 'bust',
                                id: 'bust',
                        }} />
                        <TextField
                            id="waist"
                            label="Обхват талии"
                            className={classes.textField}
                            value={context.state.params.waist || ''}
                            onChange={handleChange('waist')}
                            margin="normal"
                            inputProps={{
                                name: 'waist',
                                id: 'waist',
                        }} />
                        <TextField
                            id="hip"
                            label="Обхват бедер"
                            className={classes.textField}
                            value={context.state.params.hip || ''}
                            onChange={handleChange('hip')}
                            margin="normal"
                            inputProps={{
                                name: 'hip',
                                id: 'hip',
                        }} />
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="haircolor">Цвет волос</InputLabel>
                            <Select value={context.state.params.haircolor || ''} onChange={handleChange('haircolor')}
                                inputProps={{
                                    name: 'haircolor',
                                    id: 'haircolor',
                                }}
                            >
                                <MenuItem value='bru'>Черные</MenuItem>
                                <MenuItem value='brown'>Коричневые</MenuItem>
                                <MenuItem value='blond'>Русые</MenuItem>
                                <MenuItem value='redhead'>Рыжие</MenuItem>
                                <MenuItem value='other'>Другой</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="hairlength">Длина волос</InputLabel>
                            <Select value={context.state.params.hairlength || ''} onChange={handleChange('hairlength')}
                                inputProps={{
                                    name: 'hairlength',
                                    id: 'hairlength',
                                }}
                            >
                                <MenuItem value='xs'>Очень короткие</MenuItem>
                                <MenuItem value='sm'>Короткие</MenuItem>
                                <MenuItem value='md'>Средние</MenuItem>
                                <MenuItem value='lg'>Длинные</MenuItem>
                                <MenuItem value='xl'>Очень длинные</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="eyecolor">Цвет глаз</InputLabel>
                            <Select value={context.state.params.eyecolor || ''} onChange={handleChange('eyecolor')}
                                inputProps={{
                                    name: 'eyecolor',
                                    id: 'eyecolor',
                                }}
                            >
                                <MenuItem value='green'>Зеленый</MenuItem>
                                <MenuItem value='brown'>Карий</MenuItem>
                                <MenuItem value='blue'>Голубой</MenuItem>
                                <MenuItem value='gray'>Серый</MenuItem>
                                <MenuItem value='other'>Другой</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="appearance">Тип внешности</InputLabel>
                            <Select value={context.state.params.appearance || ''} onChange={handleChange('appearance')}
                                inputProps={{
                                    name: 'appearance',
                                    id: 'appearance',
                            }}>
                                <MenuItem value='eur'>Европейская</MenuItem>
                                <MenuItem value='asi'>Азиатская</MenuItem>
                                <MenuItem value='oth1'>Еще какая-нибудь</MenuItem>
                                <MenuItem value='oth2'>И еще</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="scenaries">Типы съемок</InputLabel>
                            <Select value={context.state.params.scenaries || ''} onChange={handleChange('scenaries')}
                                inputProps={{
                                    name: 'scenaries',
                                    id: 'scenaries',
                            }}>
                                <MenuItem value='portret'>Портретная</MenuItem>
                                <MenuItem value='fashion'>Фешн</MenuItem>
                                <MenuItem value='ero'>Ню</MenuItem>
                                <MenuItem value='street'>Уличная</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControlLabel
                            className={classes.checkbox}
                            control={
                                <Checkbox onChange={handleChangeCheckbox('workwithanimals')} checked={context.state.params.workwithanimals ? true : false} color="primary"  />
                            }
                            label="Опыт работы с животными"
                        />
                        <Button variant="contained" color="primary" className={classes.button} onClick={() => {
                            console.log(context.state);
                            axios.post('/api/updateuserinfo', {
                                uid: localStorage.getItem('uid'),
                                data: context.state
                            })
                        }}>Сохранить</Button>
                    </form>
                )
            }}
        </GlobalContext.Consumer>
    );
}
