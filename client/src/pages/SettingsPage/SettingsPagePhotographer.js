import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, MenuItem, Select, FormControl, InputLabel, Checkbox, FormControlLabel, Button } from '@material-ui/core';
import axios from 'axios';
import GlobalContext from '../../GlobalContext';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(2),
    width: '90%',
  },
  formControl: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(2),
    width: '90%',
  },
  checkbox: {
    margin: theme.spacing(3)
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
  button: {
    width: '90%',
    marginLeft: theme.spacing(3),
    color: '#fff'
  },
}));

export default function SettingsPagePhotographer() {
    const classes = useStyles();

    useEffect( () => {
        document.title = 'Настройки профиля';    
    }, [])

    return (
        <GlobalContext.Consumer>
            {context => {

                const handleChange = name => event => {
                    context.setState({
                        params: {
                            ...context.state.params,
                            [name]: event.target.value
                        }
                    })
                };

                const handleChangeCheckbox = name => event => {
                    context.setState({
                        params: {
                            ...context.state.params,
                            [name]: event.target.checked
                        }
                    })
                };                

                return (
                    <form className={classes.root} autoComplete="off">
                        <TextField
                            id="name"
                            label="Имя"
                            className={classes.textField}
                            value={context.state.params.name || ''}
                            onChange={handleChange('name')}
                            margin="normal"
                            inputProps={{
                                name: 'name',
                                id: 'name',
                        }} />
                        
                        <TextField
                            id="surname"
                            label="Фамилия"
                            className={classes.textField}
                            value={context.state.params.surname || ''}
                            onChange={handleChange('surname')}
                            margin="normal"
                            inputProps={{
                                name: 'surname',
                                id: 'surname',
                        }} />
                        
                        <TextField
                            id="age"
                            label="Возраст"
                            className={classes.textField}
                            value={context.state.params.age || ''}
                            onChange={handleChange('age')}
                            margin="normal"
                            inputProps={{
                                name: 'age',
                                id: 'age',
                        }} />
                        
                        <TextField
                            id="experience"
                            label="Опыт (лет)"
                            className={classes.textField}
                            value={context.state.params.experience || ''}
                            onChange={handleChange('experience')}
                            margin="normal"
                            inputProps={{
                                name: 'experience',
                                id: 'experience',
                        }} />
                
                        
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="scenaries">Типы съемок</InputLabel>
                            <Select value={context.state.params.scenaries || ''} onChange={handleChange('scenaries')}
                                inputProps={{
                                    name: 'scenaries',
                                    id: 'scenaries',
                            }}>
                                <MenuItem value='portret'>Портретная</MenuItem>
                                <MenuItem value='fashion'>Фешн</MenuItem>
                                <MenuItem value='ero'>Ню</MenuItem>
                                <MenuItem value='street'>Уличная</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControlLabel
                            className={classes.checkbox}
                            control={
                                <Checkbox onChange={handleChangeCheckbox('workwithanimals')} checked={context.state.params.workwithanimals ? true : false} color="primary"  />
                            }
                            label="Опыт работы с животными"
                        />
                        <Button variant="contained" color="primary" className={classes.button} onClick={() => {
                            console.log(context.state);
                            axios.post('/api/updateuserinfo', {
                                uid: localStorage.getItem('uid'),
                                data: context.state
                            })
                        }}>Сохранить</Button>
                    </form>
                )
            }}
        </GlobalContext.Consumer>
    );
}
