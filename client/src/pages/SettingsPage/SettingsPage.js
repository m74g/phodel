import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Paper } from '@material-ui/core';

import SettingsPageModel from './SettingsPageModel'
import GlobalContext from '../../GlobalContext';
import SettingsPagePhotographer from './SettingsPagePhotographer';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      color: theme.palette.text.secondary,
    },
    button: {
      width: '100%',
      color: '#fff'
    },
    specialButton: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      width: '100%',
      color: '#fff'
    }
}));

export default function SettingsPage() {
    const classes = useStyles();
    return (
      <GlobalContext.Consumer>
          {context => {
      console.log(context.state);
            return (
              <Grid container>
                <Grid item lg={3}></Grid>
                <Grid item lg={6}>
                    <Paper className={classes.paper}>
                        <Grid item container>
                            <Grid item lg={12}>
                                Настройки профиля
                                {context.state.type === "p" ? <SettingsPagePhotographer /> : <SettingsPageModel />} 
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
              </Grid>
            )
          }}
        </GlobalContext.Consumer>
    )
}
