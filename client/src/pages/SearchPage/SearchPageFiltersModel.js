import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, 
        FormControl, 
        InputLabel, 
        Select, 
        MenuItem, 
        TextField, 
        Button, 
        FormControlLabel, 
        Checkbox } from '@material-ui/core';
import axios from 'axios';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    formControl: {
        margin: theme.spacing(0),
        width: '90%',
    },
    textField: {
        margin: theme.spacing(0)
    },
    button: {
      width: '100%',
      color: '#fff',
      marginTop: theme.spacing(2)
    }
}));

export default function SearchPageFiltersModel() {
    const classes = useStyles();
    const [state, setState] = React.useState({});
    const [values, setValues] = React.useState({
        haircolor: '',
        eyecolor: '',
        hairlength: '',
        appearance: '',
        scenaries: '',
        height: '',
        bust: '',
        waist: '',
        hip: '',
        workwithanimals: false
    });

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };
    
    const handleChangeCheckbox = name => event => {
        setValues({ ...values, [name]: event.target.checked });
    };

    const updateState = (data) => {
        setState({ ...state, ...data });
    };

    useEffect(() => {       
        axios.post('/api/getuserinfo', {
            uid: localStorage.getItem('uid')
        })
        .then(res => {
            let data = res.data;
            updateState(data);
        })
    }, [])

    return (
        <Grid container>
            <Grid item xs={12}>
                <form className={classes.root} autoComplete="off">
                    <Grid container alignItems="center">
                        <Grid item xs={6} md={3}>
                            <TextField
                                id="height"
                                label="Рост (от)"
                                className={classes.textField}
                                value={values.height}
                                onChange={handleChange('height')}
                                margin="normal"
                                inputProps={{
                                    name: 'height',
                                    id: 'height',
                            }} />
                        </Grid>
                        <Grid item xs={6} md={3}>
                            <TextField
                                id="bust"
                                label="Обхват груди"
                                className={classes.textField}
                                value={values.bust}
                                onChange={handleChange('bust')}
                                margin="normal"
                                inputProps={{
                                    name: 'bust',
                                    id: 'bust',
                            }} />
                        </Grid>
                        <Grid item xs={6} md={3}>
                            <TextField
                                id="waist"
                                label="Обхват талии"
                                className={classes.textField}
                                value={values.waist}
                                onChange={handleChange('waist')}
                                margin="normal"
                                inputProps={{
                                    name: 'waist',
                                    id: 'waist',
                            }} />
                        </Grid>
                        <Grid item xs={6} md={3}>
                            <TextField
                                id="hip"
                                label="Обхват бедер"
                                className={classes.textField}
                                value={values.hip}
                                onChange={handleChange('hip')}
                                margin="normal"
                                inputProps={{
                                    name: 'hip',
                                    id: 'hip',
                            }} />
                        </Grid>
                        <Grid item xs={6} md={3}>
                            <FormControl className={classes.formControl}>
                                <InputLabel htmlFor="haircolor">Цвет волос</InputLabel>
                                <Select value={values.haircolor} onChange={handleChange('haircolor')}
                                    inputProps={{
                                        name: 'haircolor',
                                        id: 'haircolor',
                                    }}
                                >
                                    <MenuItem value='bru'>Брюнетка</MenuItem>
                                    <MenuItem value='brown'>Шатенка</MenuItem>
                                    <MenuItem value='blond'>Блондинка</MenuItem>
                                    <MenuItem value='redhead'>Рыжая</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={6} md={3}>
                            <FormControl className={classes.formControl}>
                                <InputLabel htmlFor="hairlength">Длина волос</InputLabel>
                                <Select value={values.hairlength} onChange={handleChange('hairlength')}
                                    inputProps={{
                                        name: 'hairlength',
                                        id: 'hairlength',
                                    }}
                                >
                                    <MenuItem value='xs'>Очень короткие</MenuItem>
                                    <MenuItem value='sm'>Короткие</MenuItem>
                                    <MenuItem value='md'>Средние</MenuItem>
                                    <MenuItem value='lg'>Длинные</MenuItem>
                                    <MenuItem value='xl'>Очень длинные</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={6} md={3}>
                            <FormControl className={classes.formControl}>
                                <InputLabel htmlFor="eyecolor">Цвет глаз</InputLabel>
                                <Select value={values.eyecolor} onChange={handleChange('eyecolor')}
                                    inputProps={{
                                        name: 'eyecolor',
                                        id: 'eyecolor',
                                    }}
                                >
                                    <MenuItem value='green'>Зеленый</MenuItem>
                                    <MenuItem value='brown'>Карий</MenuItem>
                                    <MenuItem value='blue'>Голубой</MenuItem>
                                    <MenuItem value='gray'>Серый</MenuItem>
                                    <MenuItem value='other'>Другой</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={6} md={3}>
                            <FormControl className={classes.formControl}>
                                <InputLabel htmlFor="appearance">Тип внешности</InputLabel>
                                <Select value={values.appearance} onChange={handleChange('appearance')}
                                    inputProps={{
                                        name: 'appearance',
                                        id: 'appearance',
                                }}>
                                    <MenuItem value='eur'>Европейская</MenuItem>
                                    <MenuItem value='asi'>Азиатская</MenuItem>
                                    <MenuItem value='oth1'>Еще какая-нибудь</MenuItem>
                                    <MenuItem value='oth2'>И еще</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={6} md={3}>
                            <FormControl className={classes.formControl}>
                                <InputLabel htmlFor="scenaries">Типы съемок</InputLabel>
                                <Select value={values.scenaries} onChange={handleChange('scenaries')}
                                    inputProps={{
                                        name: 'scenaries',
                                        id: 'scenaries',
                                }}>
                                    <MenuItem value='portret'>Портретная</MenuItem>
                                    <MenuItem value='fashion'>Фешн</MenuItem>
                                    <MenuItem value='ero'>Ню</MenuItem>
                                    <MenuItem value='street'>Уличная</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={6} md={6}>
                            <FormControlLabel
                                control={
                                    <Checkbox className={classes.checkbox} checked={values.workwithanimals} onChange={handleChangeCheckbox('workwithanimals')} value="workwithanimals" color="primary"  />
                                }
                                label="Опыт работы с животными"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Button variant="contained" color="primary" className={classes.button} >Отфильтровать</Button>
                        </Grid>
                    </Grid>
                </form>
            </Grid>
        </Grid>
    )
}
