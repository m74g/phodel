import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Paper, Tabs, Tab, Typography, Box } from '@material-ui/core';

import SearchPageFiltersModel from './SearchPageFiltersModel';
import SearchPageFiltersPhotographer from './SearchPageFilterPhotographer';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      color: theme.palette.text.secondary,
    },
    button: {
      width: '100%',
      color: '#fff'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 140,
    },
    selectEmpty: {
    marginTop: theme.spacing(2),
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
    tabs: {
        width: '100%'
    }
}));

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`nav-tabpanel-${index}`}
        aria-labelledby={`nav-tab-${index}`}
        {...other}
      >
        <Box p={3}>{children}</Box>
      </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-tabpanel-${index}`,
};
}

function LinkTab(props) {
return (
    <Tab
        component="a"
        onClick={event => {
            event.preventDefault();
        }}
        {...props}
    />
);
}

export default function SearchPageFilters() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Grid container>
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <Tabs
                        variant="fullWidth"
                        value={value}
                        onChange={handleChange}
                        aria-label="nav tabs"
                    >
                        <LinkTab label="📸" href="/drafts" {...a11yProps(0)} />
                        <LinkTab label="💁‍♀️" href="/trash" {...a11yProps(1)} />      
                    </Tabs>
                    <TabPanel value={value} index={0}>
                        <SearchPageFiltersPhotographer />
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <SearchPageFiltersModel />
                    </TabPanel>
                </Paper>
            </Grid>
        </Grid>
    )
}