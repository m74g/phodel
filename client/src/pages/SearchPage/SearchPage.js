import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import axios from 'axios';

import SearchPageProfile from './SearchPageProfile';
import SearchPageFilters from './SearchPageFilters';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

const createProfile = params => {
  return (
    <SearchPageProfile  />
  )
}

export default function SearchPage() {
  const classes = useStyles();
  const [profiles, setProfiles] = useState()

  useEffect(() => {
    axios.get('/api/getusers')
    .then(res => {
      let p = [];
      res.data.users.map((d, i) => {
        p.push(
          <Grid key={i} item xs={12} md={6} lg={3}>
            <SearchPageProfile name={d.params.name + ' ' + d.params.surname} avatar={d.defaultavatar} />
          </Grid>
        )
      })
      setProfiles(p)
    })
  }, [])
  
  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <SearchPageFilters />
        {profiles}
      </Grid>
    </div>
  );
}
