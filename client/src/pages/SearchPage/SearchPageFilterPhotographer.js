import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, 
    FormControl, 
    InputLabel, 
    Select, 
    MenuItem, 
    TextField, 
    Button, 
    FormControlLabel, 
    Checkbox } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    formControl: {
        margin: theme.spacing(0),
        width: '90%'
    },
    textField: {
        margin: theme.spacing(0)
    },
    checkbox: {
        margin: theme.spacing(0)
    },
    button: {
      width: '100%',
      color: '#fff',
      marginTop: theme.spacing(2)
    }
}));

export default function SearchPageFiltersModel() {
    const classes = useStyles();
    const [values, setValues] = React.useState({
        scenaries: '',
        expirience: '',
        workwithanimals: false
    });

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    const handleChangeCheckbox = name => event => {
        setValues({ ...values, [name]: event.target.checked });
    };

    return (
        <Grid container>
            <Grid item xs={12}>
                <form className={classes.root} autoComplete="off">
                    <Grid container alignItems="center">
                        <Grid item xs={6} md={6}>
                            <FormControl className={classes.formControl}>
                                <InputLabel htmlFor="scenaries">Типы съемок</InputLabel>
                                <Select value={values.scenaries} onChange={handleChange('scenaries')}
                                    inputProps={{
                                        name: 'scenaries',
                                        id: 'scenaries',
                                }}>
                                    <MenuItem value='portret'>Портретная</MenuItem>
                                    <MenuItem value='fashion'>Фешн</MenuItem>
                                    <MenuItem value='ero'>Ню</MenuItem>
                                    <MenuItem value='street'>Уличная</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={6} md={6}>
                            <TextField
                                id="expirience"
                                label="Опыт (лет)"
                                className={classes.textField}
                                value={values.expirience}
                                onChange={handleChange('expirience')}
                                margin="normal"
                                inputProps={{
                                    name: 'expirience',
                                    id: 'expirience',
                            }} />
                        </Grid>
                        <Grid item xs={6} md={6}>
                            <FormControlLabel
                                control={
                                    <Checkbox className={classes.checkbox} checked={values.workwithanimals} onChange={handleChangeCheckbox('workwithanimals')} value="workwithanimals" color="primary"  />
                                }
                                label="Опыт работы с животными"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Button variant="contained" color="primary" className={classes.button} >Отфильтровать</Button>
                        </Grid>
                    </Grid>
                </form>
            </Grid>
        </Grid>
    )
}
