import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Grid, Button, Modal, Backdrop, Fade, Paper } from '@material-ui/core';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import GlobalContext from '../../GlobalContext';

const useStyles = makeStyles(theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      width: '100%'
    },
    dense: {
      marginTop: theme.spacing(2),
    },
    menu: {
      width: 200,
    },
    button: {
        width: '100%',
        color: '#fff'
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: '#333',
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        color: '#fff'
    }
}));

export default function SignIn() {
    const classes = useStyles();
    const history = useHistory();

    const [open, setOpen] = React.useState(false);

    const [values, setValues] = React.useState({
        email: '',
        password: '',
        modalText: 'q'
    });


    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    const handleShowModal = () => {
        setOpen(true)
    };

    const handleHideModal = () => {
        setOpen(false)
    };

    const setModalText = text => {
        setValues({modalText: text})
    };

    const validate = () => {
        for(let i in values) {
            if(values[i] === '') {
                return false;
            }
        }
        return true;
    }   

    return (
        <GlobalContext.Consumer>
            {context => {
                return (
                    <>
                        <form className={classes.container} noValidate autoComplete="off">
                            <Grid container lg={12} spacing={3}>
                                <Grid item lg={12} xs={12}>
                                    <TextField
                                        id="outlined-email-input-signin"
                                        label="Email"
                                        className={classes.textField}
                                        onChange={handleChange('email')}
                                        type="email"
                                        name="email-signin"
                                        autoComplete="email"
                                        margin="normal"
                                    />
                                </Grid>
                                <Grid item lg={12} xs={12}>
                                    <TextField
                                        id="outlined-password-input-signin"
                                        label="Пароль"
                                        className={classes.textField}
                                        onChange={handleChange('password')}
                                        type="password"
                                        autoComplete="current-password"
                                        margin="normal"
                                    />
                                </Grid>
                                <Grid item lg={12} xs={12}>
                                    <Button color="primary" variant="contained" className={classes.button} onClick={()=>{                                        
                                        if (validate()) {
                                            axios.post('/api/signin', {
                                                email: values.email,
                                                password: values.password
                                            })
                                            .then(res => {                                                   
                                                if (res.data.status === 'success') {
                                                    localStorage.setItem('isl', true)
                                                    localStorage.setItem('uid', res.data.uid)
                                                    context.setState({
                                                        name   : res.data.name,
                                                        surname: res.data.surname,
                                                        login  : res.data.login,
                                                        params : res.data.params,
                                                        type   : res.data.type,
                                                        avatar : res.data.avatar,
                                                        uid    : res.data.uid
                                                    })
                                                    history.push("/profile")
                                                } else {
                                                    setModalText(res.data.text);
                                                    handleShowModal()
                                                }
                                            })
                                        } else {
                                            setModalText('Заполните, пожалуйста, все поля💩');
                                            handleShowModal();
                                        }
                                    }}>
                                        Войти
                                    </Button>
                                </Grid>
                            </Grid>
                        </form>
                        <Modal
                            aria-labelledby="transition-modal-title"
                            aria-describedby="transition-modal-description"
                            className={classes.modal}
                            open={open}
                            onClose={handleHideModal}
                            closeAfterTransition
                            BackdropComponent={Backdrop}
                            BackdropProps={{
                            timeout: 500,
                            }}
                        >
                            <Fade in={open}>
                                <Paper className={classes.paper}>
                                    <h2 id="transition-modal-title">Авторизация</h2>
                                    <p id="transition-modal-description">{values.modalText}</p>
                                </Paper>
                            </Fade>
                        </Modal>
                    </>
                )
            }}
        </GlobalContext.Consumer>
    )
}
