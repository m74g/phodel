import React from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { MenuItem, TextField, Grid, Button, Modal, Backdrop, Fade, Paper } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

const types = [
    {
      value: 'p',
      label: '📸 Фотограф',
    },
    {
      value: 'm',
      label: '💁‍♀️ Модель',
    }
]

const useStyles = makeStyles(theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      width: '100%'
    },
    dense: {
      marginTop: theme.spacing(2),
    },
    menu: {
      width: 200,
    },
    button: {
        width: '100%',
        color: '#fff'
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: '#333',
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        color: '#fff'
    }
}));

export default function SignUp() {
    const classes = useStyles();
    const history = useHistory();

    const [open, setOpen] = React.useState(false);

    const [values, setValues] = React.useState({
        name: '',
        surname: '',
        email: '',
        password: '',
        type: '',
        modalText: 'q'
    });

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    const handleShowModal = () => {
        setOpen(true)
    };

    const handleHideModal = () => {
        setOpen(false)
    };

    const setModalText = text => {
        setValues({modalText: text})
    };

    const validate = () => {
        for(let i in values) {
            if(values[i] === '') {
                return false;
            }
        }
        return true;
    }

    return (
        <>
            <form className={classes.container} noValidate autoComplete="off">
                <Grid container spacing={3}>
                    <Grid item lg={12} xs={12}>
                        <TextField
                            id="outlined-name-signup"
                            label="Имя"
                            className={classes.textField}
                            onChange={handleChange('name')}
                            margin="normal"
                        />
                    </Grid>
                    <Grid item lg={12} xs={12}>
                        <TextField
                            id="outlined-surname-signup"
                            label="Фамилия"
                            className={classes.textField}
                            onChange={handleChange('surname')}
                            margin="normal"
                        />
                    </Grid>
                    <Grid item lg={12} xs={12}>
                        <TextField
                            id="outlined-email-input-signup"
                            label="Email"
                            className={classes.textField}
                            onChange={handleChange('email')}
                            type="email"
                            name="email-signup"
                            autoComplete="email"
                            margin="normal"
                        />
                    </Grid>
                    <Grid item lg={12} xs={12}>
                        <TextField
                            id="outlined-password-input-signup"
                            label="Пароль"
                            className={classes.textField}
                            onChange={handleChange('password')}
                            type="password"
                            autoComplete="current-password"
                            margin="normal"
                        />
                    </Grid>
                    <Grid item lg={12} xs={12}>
                        <TextField
                            id="outlined-select-type-signup"
                            select
                            label="Тип"
                            className={classes.textField}
                            value={values.type}
                            onChange={handleChange('type')}
                            SelectProps={{
                                MenuProps: {
                                    className: classes.menu,
                                }
                            }}
                            helperText="Выберите тип профиля"
                            margin="normal"
                        >
                            {types.map(option => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item lg={12} xs={12}>
                        <Button color="primary" variant="contained" className={classes.button} onClick={() => {
                            if(validate()) {
                                axios.post('/api/signup', {
                                    name    : values.name,
                                    surname : values.surname,
                                    email   : values.email,
                                    password: values.password,
                                    type    : values.type
                                })
                                .then(res => {            
                                    if (res.data.status === 'success') {
                                        localStorage.setItem('isl', true)
                                        localStorage.setItem('uid', res.data.uid)
                                        history.push("/profile")
                                        window.location.reload();
                                    } else {
                                        setModalText(res.data.text);
                                        handleShowModal()
                                    }
                                })
                            } else {
                                setModalText('Заполните, пожалуйста, все поля💩');
                                handleShowModal();
                            }
                        }}>
                            Зарегистрироваться
                        </Button>
                    </Grid>
                </Grid>
            </form>
            <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={open}
                    onClose={handleHideModal}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                    timeout: 500,
                    }}
                >
                    <Fade in={open}>
                        <Paper className={classes.paper}>
                            <h2 id="transition-modal-title">Регистрация</h2>
                            <p id="transition-modal-description">{values.modalText}</p>
                        </Paper>
                    </Fade>
            </Modal>
        </>
    )
}
