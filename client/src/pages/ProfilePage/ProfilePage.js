import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Button } from '@material-ui/core';
import { useHistory } from 'react-router-dom';


import ProfilePagePhotos from './ProfilePagePhotos';
import ProfilePageAccount from './ProfilePageAccount';
import GlobalContext from '../../GlobalContext';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      color: theme.palette.text.secondary,
    },
    button: {
      width: '100%',
      color: '#fff'
    },
    specialButton: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      width: '100%',
      color: '#fff'
    }
}));

export default function ProfilePage() {
    const classes = useStyles();
    const history = useHistory();

    return (
      <GlobalContext.Consumer>
        {context => {
          return (
            <div className={classes.root}>
              <Grid container spacing={3}>
                <ProfilePageAccount />  
              </Grid>
              
              <Grid container spacing={3}>
                <Grid item xs={4}>
                  <Button variant="contained" color="primary" className={classes.specialButton} onClick={() => {
                    history.push("/upgrade")
                  }}>Level up<span role="img" aria-label="rocket">🚀</span></Button>
                </Grid>
                <Grid item xs={4}>
                  <Button variant="contained" color="primary" className={classes.button} onClick={() => {
                    history.push("/settings")
                  }}>Настройки</Button>
                </Grid>
                <Grid item xs={4}>
                  <Button variant="contained" color="secondary" className={classes.button} onClick={()=>{
                    localStorage.clear();
                    history.push("/");
                  }}>Выход</Button>
                </Grid>
              </Grid>

              <Grid container spacing={2}>
                <ProfilePagePhotos />
              </Grid>
            </div>
          )
        }}
      </GlobalContext.Consumer>
    )
}
