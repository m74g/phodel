import React from 'react';
import { Paper, Grid, Box, makeStyles } from '@material-ui/core';

import Avatar from '../../components/Avatar';
import GlobalContext from '../../GlobalContext';

export default function ProfilePageAccount() {    
    const useStyles = makeStyles(theme => ({
        root: {
          flexGrow: 1,
        },
        paper: {
          padding: theme.spacing(2),
          color: theme.palette.text.secondary,
        },
        button: {
          width: '100%',
          color: '#fff'
        },
        specialButton: {
          background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
          width: '100%',
          color: '#fff'
        }
    }));

    return (
        <GlobalContext.Consumer>
            {context => {                
                return (
                    <>
                        <Grid item xs={2}>
                            <Avatar size="lg" imgSrc={context.state.defaultAvatar} />
                        </Grid>
                        <Grid item xs={10}>
                            <Paper >
                                <Grid item xs={8}>
                                    <Box component="div">{context.state.params.name ? context.state.params.name + ' ' + context.state.params.surname : 'Loading...'}</Box>
                                </Grid>
                            </Paper>
                        </Grid>
                    </>
                )
            }}
        </GlobalContext.Consumer>
    )
}
