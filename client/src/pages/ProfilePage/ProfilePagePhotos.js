import React from 'react';
import { Grid } from '@material-ui/core';

const imageUrl = 'https://www.solomonedwards.com/wp-content/uploads/2017/01/life_through_the_lens_by_kabdul-d8hf6xr.jpg';

export default function ImageGridList() {
  let images = [];

  for(let i = 0; i < 15; i++) {
    images.push(
      <Grid key={i} item xs={6} md={4}>
          <img src={imageUrl} width="100%" alt={new Date().getTime()} />
      </Grid>
    )
  }

  return (
      <>{images}</>
  );
}
