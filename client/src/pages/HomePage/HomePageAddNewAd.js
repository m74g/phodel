import React from 'react';
import { makeStyles, Button, Modal } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import HomePageAddNewAdForm from './HomePageAddNewAdForm';

const useStyles = makeStyles(theme => ({
    button: {
        margin: '.1rem 0',
        width: '100%',
        color: '#fff'
    },
    paper: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '50vmax',
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
      },
}));



export default function HomePageAddNewAd() {
    const classes = useStyles();
    
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <>
            <Button variant="contained" color="primary" className={classes.button} onClick={handleOpen}>
                <AddIcon />
            </Button>

            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={open}
                onClose={handleClose}
            >
                <div className={classes.paper}>
                    <HomePageAddNewAdForm />
                </div>
            </Modal>
        </>
    )
}
