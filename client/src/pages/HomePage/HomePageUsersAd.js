import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

function ListItemLink(props) {
  return (
    <ListItem key={props.key} button component="a">
        <ListItemText primary={props.key % 2 === 0 ? "📸 Ищу модель {params}" : "💁‍♀️ Ищу фотографа {params}"} />
    </ListItem>
  );
}

export default function HomePageUsersAd() {
  const classes = useStyles();
  let ads = [];

  for(let i = 0; i < 25; i++) {
      ads.push(ListItemLink({key: i}))
  }

  return (
    <div className={classes.root}>
      <List component="nav">
        {ads}
      </List>
    </div>
  );
}
