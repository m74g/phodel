import React from 'react';
import { makeStyles, Grid, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import { KeyboardDatePicker } from '@material-ui/pickers';

import GlobalContext from '../../GlobalContext';

const useStyles = makeStyles(theme => ({
    formControl: {
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(2),
        marginTop: theme.spacing(2),
        width: '90%',
    },
}));

const PhotoTypeSelect = () => {
    const classes = useStyles();

    const handleChange = name => event => {
        
    };

    return (
        <FormControl className={classes.formControl}>
            <InputLabel htmlFor="scenaries">Тип съемок</InputLabel>
            <Select value={''}
                inputProps={{
                    name: 'scenaries',
                    id: 'scenaries',
            }}>
                <MenuItem value='portret'>Портретная</MenuItem>
                <MenuItem value='fashion'>Фешн</MenuItem>
                <MenuItem value='ero'>Ню</MenuItem>
                <MenuItem value='street'>Уличная</MenuItem>
            </Select>
        </FormControl>
    )
}

// const DatePicker = () => {
//     const classes = useStyles();

//     return (
//         <KeyboardDatePicker
//           disableToolbar
//           variant="inline"
//           format="MM/dd/yyyy"
//           margin="normal"
//           id="date-picker-inline"
//           label="Date picker inline"
//           value={selectedDate}
//           onChange={handleDateChange}
//           KeyboardButtonProps={{
//             'aria-label': 'change date',
//           }}
//         />
//     )
// }

export default function HomePageAddNewAdForm() {
    return (
        <GlobalContext.Consumer>
            {context => {
                console.log(context);
                
                return (
                    <Grid container>
                        <Grid item xs={12}>
                            Ищу {context.state.type === 'p' ? 'модель' : 'фотографа'} для {true ? '{тип съемки}' : ''} фотосъемки
                        </Grid>
                        <Grid item xs={12}>
                            <PhotoTypeSelect />
                        </Grid>
                    </Grid>
                )
            }}
        </GlobalContext.Consumer>
    )
}
