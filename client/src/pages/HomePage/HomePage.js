import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Grid } from '@material-ui/core';
import { Link } from 'react-router-dom';

import HomePageUsersAd from './HomePageUsersAd';
import HomePageAddNewAd from './HomePageAddNewAd';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }
}));

export default function HomePage() {
    const classes = useStyles();

    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item lg={12} xs={12}>
            <Paper className={classes.paper}>
                Добро пожаловать на Phodel.&nbsp;
                {localStorage.getItem('isl') ? '' : <Link to='/auth'>Авторизоваться</Link>}
            </Paper>
          </Grid>
          <Grid item lg={12} xs={12}>
            <HomePageAddNewAd />
          </Grid>
          <Grid item lg={12} xs={12}>
            <Paper className={classes.paper}>
              <HomePageUsersAd />
            </Paper>
          </Grid>
        </Grid>
      </div>
    )
}
