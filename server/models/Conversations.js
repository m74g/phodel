const { Schema, model } = require('mongoose');

const schema = new Schema({
    owner: {
        type: String,
        required: true
    },
    parthner: {
        type: String,
        required: true
    },
    history: {
        type: Array,
        default: [],
        required: true
    }
})

module.exports = model('Conversations', schema, 'conversations');