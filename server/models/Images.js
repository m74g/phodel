const { Schema, model } = require('mongoose');

const schema = new Schema({
    owner: {
        type: String,
        required: true
    },
    images: {
        type: Array,
        required: true
    }
})

module.exports = model('Images', schema, 'images');