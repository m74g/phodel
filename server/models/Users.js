const { Schema, model } = require('mongoose');

const schema = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    phone: {
        type: String,
    },
    login: {
        type: String
    },
    subLevel: {
        type: String,
        default: 'free'
    },
    avatar: {
        picture: { content: Buffer, contentType: String }
    },
    defaultAvatar: {
        type: String
    },
    params: {
        type: Object,
        default: {
            name: {
                type: String,
                required: true
            },
            surname: {
                type: String,
                required: true
            }
        }
    },
    uid: {
        type: String,
        default: new Date().getTime() + ''
    }
})

module.exports = model('User', schema, 'users');