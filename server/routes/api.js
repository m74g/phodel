const { Router } = require('express');
const User = require('../models/Users');

const router = Router();
const s = {
    ok: 'success',
    err: 'error'
}

const responser = (status, data, text) => {
    let timestamp = new Date();
    let result = {
        status: status,
        text: text,
        timestamp: timestamp
    };
    switch (status) {
        case 'success': 
            return {...result, ...data};

        case 'error':
            return result;
    
        default:
            throw new Error('No data')
    }
}

router.post('/signin', async (req, res) => {
    let timestamp = new Date();
    if (await User.exists({email: req.body.email, password: req.body.password})) {
        User.find((err, users) => {
            users.map(user => {
                if (req.body.email === user.email) {
                    res.status(200).json({
                        uid: user.uid,
                        params: user.params,
                        type: user.type,
                        status: s.ok,
                        text: 'Авторизация прошла успешно😌',
                        timestamp: timestamp
                    });
                }
            })
        });
    } else {
        res.status(200).json({
            status: s.err,
            text: 'Логин или пароль введены не верно😕',
            timestamp: timestamp
        });
    }
});

router.post('/signup', async (req, res) => {
    let timestamp = new Date();
    if (await User.exists({email: req.body.email})) {
        res.status(200).json({
            status: s.err,
            text: 'Пользователь с таким email уже зарегистрирован🤔',
            timestamp: timestamp
        });
    } else {
        const newUser = new User({
            params: {
                name   : req.body.name,
                surname: req.body.surname,
            },
            email        : req.body.email,
            password     : req.body.password,
            type         : req.body.type,
            defaultAvatar: `https://eu.ui-avatars.com/api/?name=${req.body.name}+${req.body.surname}&size=350&color=ff69b4`
        })
    
        await newUser.save();
        res.status(200).json({
            status: s.ok,
            uid: newUser.uid,
            text: 'Вы успешно зарегистрировались☺',
            timestamp: timestamp
        });
    }
});

router.post('/getuserinfo', async (req, res) => {      
    await User.find({uid: req.body.uid}, (err, users) => {
        if(err) res.status(500);
        users = users[0]
        res.status(200).json(responser('success', {
            uid: users.uid,
            type: users.type,
            params: users.params || {},
            avatar: users.avatar || 'http://takeaseathome.com/media/images/avatar-placeholder.png',
            defaultAvatar: users.defaultAvatar,
        }, 'Информация найдена'))
    })
})

router.post('/updateuserinfo', async (req, res) => {       
        await User.findOneAndUpdate(
            {uid: req.body.uid}, 
            {
                params: {
                    name           : req.body.data.params.name,
                    surname        : req.body.data.params.surname,
                    age            : req.body.data.params.age,
                    height         : req.body.data.params.height,
                    weight         : req.body.data.params.weight,
                    bust           : req.body.data.params.bust,
                    waist          : req.body.data.params.waist,
                    hip            : req.body.data.params.hip,
                    haircolor      : req.body.data.params.haircolor,
                    hairlength     : req.body.data.params.hairlength,
                    eyecolor       : req.body.data.params.eyecolor,
                    appearance     : req.body.data.params.appearance,
                    scenaries      : req.body.data.params.scenaries,
                    workwithanimals: req.body.data.params.workwithanimals
                },
                avatar: req.body.data.avatar
            }, 
            {upsert: true}, 
            (err, doc) => {
                if (err) return res.status(500).json(responser('error', {}, 'Ошибка сервера'));
                return res.status(200).json(responser('success', {}, 'Данные успешно обновлены'));
            })
})

router.get('/getusers', async (req, res) => {
    let users = [];   
    User.find((err, docs) => {        
        if (err) return res.status(500).json(responser('error', {}, 'Ошибка сервера'));
        docs.map(doc => {
            users.push({
                sublevel: doc.subLevel,
                uid: doc.uid,
                type: doc.type,
                defaultavatar: doc.defaultAvatar,
                avatar: doc.avatar,
                params: doc.params
            })
        })
        return res.status(200).json(responser('success', {
            users: users
        }, 'Данные получены'));
    })
})

module.exports = router;