/**
 * phodel
 * version 0.1
 */

//libs
const express    = require('express');
const bodyParser = require('body-parser');
const mongoose   = require('mongoose');
const path       = require('path');

//modules
const apiRoutes = require('./routes/api');


//consts
const PORT    = process.env.PORT || 80;
const app     = express();
const DB_USER = 'home';
const DB_PASS = 'oP2a4Cc5t6';

//express config
app.use(express.static(path.join(__dirname, '/public')));
app.use(express.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use('/api/', apiRoutes);

let init = async () => {
    try {
        await mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASS}@cluster0-eztwm.mongodb.net/test`, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        });
        app.listen(PORT)
    } catch (e) {
        console.log(e);
    }
}

init();

app.get('*', (req, res) => {
    res.status(200).sendFile(path.join(__dirname + '/public/index.html'))
})